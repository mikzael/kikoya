import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {user} from '../data/users'


export default function Login({ setToken }) {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();

    const handleSubmit = async e => {
        e.preventDefault();

        // aqui va la llamada al endpoint pero en su lugar emularemos la respuesta
        // -----------------------------------------------------------------------

            user.map(u => {
                if(u.username == username && u.password == password){ 
                    setToken(u.id + ":" + u.username + ":" + u.role);
                }
            });


        // -----------------------------------------------------------------------

      }    

        return (
        <div className="auth-wrapper">
            <div className="auth-inner">          
                <form onSubmit={handleSubmit}>
                    <h3>Iniciar Sesión</h3>
                    <div className="form-group">
                        <label>Usuario</label>
                        <input value={username} onChange={e => setUserName(e.target.value)} className="form-control" placeholder="Nombre de Usuario" />
                    </div>
                    <div className="form-group">
                        <label>Contraseña</label>
                        <input type="password" value={password} onChange={e => setPassword(e.target.value)} className="form-control" placeholder="Contraseña" />
                    </div>
                    <button type="submit" className="btn btn-primary btn-block">Iniciar</button>
                </form>
            </div>
        </div>
        );
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
  };