import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Login from './components/Login';
import Shop from './containers/Shop';
import {NavLink, withRouter}  from 'react-router-dom'


const App = () => {

    const [token, setToken] = useState();
    const [rol, setRol] = useState();

    if(!token) {
        return <Login setToken={setToken} />
    }

    return (

    <Router>
        <div className="App">
            <Switch>
                <Route exact path='/' component={() => <Shop token={token} />} />
                <Route path="/login" component={Login} />
            </Switch>
        </div>
    </Router>


    );
}

export default App;
