import React, { useState, useEffect } from 'react';
import Cart from './Cart';
import ProductList from './ProductList';
import {role_group} from '../data/role_grants'




const Shop = ({ token }) => {

    const [role, setRole] = useState();

    useEffect(() => 
    {

        const DisplayNav = () => {
        
            // aqui va la llamada al endpointpara renovar el token y para obtener 
            // el detalle del rol.
            // -----------------------------------------------------------------------
        
                let temp = token.split(":");
                temp = temp[2];
        
                role_group.map(r => {
                    if(r.role == temp){ 
                        setRole(r.modules);
                    }
                });
        
            // -----------------------------------------------------------------------
        
        }  

        DisplayNav()
    }, []);
  


    return (

        <>

        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <h1>Virtual Shop</h1>
                </div>
            </div>
            <div className="row">
                <div className="col-md-8">
                    <ProductList />
                </div>
                <div className="col-md-4">
                    <Cart />
                </div>
            </div>

            <footer>
                <small>
                    powered by <a href="http://www.kikoya.mx/">Kikoya</a>
                </small>
            </footer>
        </div>
        </>
    );
}

export default Shop;
